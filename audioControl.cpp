#include "audioControl.h"
#include "Windows.h"
#include "Audiopolicy.h"
#include "Audioclient.h"
#include "Mmdeviceapi.h"
#include "Unknwn.h"
#include <objbase.h>

#include <stack>

/*Customized stack that allows for easy, exception safe releasing of window resources. Use castPush to add an item. The item must inherit IUnknown (Calling release on the underlying object must be valid).
 */
class ReleaseStack : public std::stack<IUnknown**>{
public:
	~ReleaseStack(){
		while(size()){
			auto obj = top();
			if(obj && *obj){		//In case someone already released it
				(**obj).Release();
				*obj = nullptr;
			}
			pop();
		}
	};

	//Auto cast to IUnknown
	template<typename T>
	void castPush(T obj){
		push((IUnknown**)(obj));
	};
};

//Matches the application volume level with the master volume lvel
void levelVolumes(){
	ReleaseStack relStack{};	//Handle the calling of Release() on resources.
	
	HRESULT hr;
	IMMDeviceEnumerator* pDeviceEnumerator = nullptr;	
	relStack.castPush(&pDeviceEnumerator);

	//Create device enumerator
	hr = CoCreateInstance((CLSID)__uuidof(MMDeviceEnumerator), nullptr, CLSCTX_ALL, (IID)__uuidof(IMMDeviceEnumerator), (void**)&pDeviceEnumerator);
	if(FAILED(hr)){throw hr;}
	
	//Get the default audio endpoint
	IMMDevice* pMMDevice = nullptr;	
	relStack.castPush(&pMMDevice);
	hr = pDeviceEnumerator->GetDefaultAudioEndpoint((EDataFlow)eRender, (ERole)eMultimedia, &pMMDevice);
	if(FAILED(hr)){throw hr;}

	//Create the session manager
	IAudioSessionManager2* pAudioSessionManager2 = nullptr;	
	relStack.castPush(&pAudioSessionManager2);
	hr = pMMDevice->Activate((REFIID)__uuidof(IAudioSessionManager2), (DWORD)CLSCTX_ALL, nullptr, (void**)&pAudioSessionManager2);
	if(FAILED(hr)){throw hr;}

	//Create the session enumerator
	IAudioSessionEnumerator* pAudioSessionEnumerator = nullptr;	
	relStack.castPush(&pAudioSessionEnumerator);
	hr = pAudioSessionManager2->GetSessionEnumerator(&pAudioSessionEnumerator);
	if(FAILED(hr)){throw hr;}
	
	//Get the number of audio sessions
	int sessionCount = 0;
	hr = pAudioSessionEnumerator->GetCount(&sessionCount);
	if(FAILED(hr)){throw hr;}

	//For each audio session, set vol to master
	IAudioSessionControl* pAudioSessionControl = nullptr;	
	ISimpleAudioVolume* pSimpleAudioVolume = nullptr;	
	

	for(int i=0; i<sessionCount; i++){
		ReleaseStack innerRelStack{};
		innerRelStack.castPush(&pAudioSessionControl);
		innerRelStack.castPush(&pSimpleAudioVolume);

		//Get the session controller
		hr = pAudioSessionEnumerator->GetSession((int)i, &pAudioSessionControl);
		if(FAILED(hr)){throw hr;}

		//Get the volume controller
			//Ugly unsupported way to get an ISimpleAudioVolume from an IAudioSessionControl
		hr = pAudioSessionControl->QueryInterface(__uuidof(ISimpleAudioVolume), (void**)&pSimpleAudioVolume);
		if(FAILED(hr)){throw hr;}
		
		//Set the volume
		hr = pSimpleAudioVolume->SetMasterVolume((float)1.0, (LPCGUID)nullptr);
		if(FAILED(hr)){throw hr;}
	}
	return;
}


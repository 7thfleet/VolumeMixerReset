#include "audioControl.h"
#include <iostream>
#include "Windows.h"
#include "comdef.h"
#include <objbase.h>

int main(int argc, const char * argv[]){
	try{
		HRESULT hr = CoInitialize(nullptr);
		if(FAILED(hr)){throw hr;} 
		levelVolumes();
	}
	catch(HRESULT e){
		_com_error error(e);
		std::cerr << "RAW: " << e << std::endl;
		std::cerr << "com error: " << (LPCTSTR)(error.ErrorMessage()) << std::endl;
	}
	return 0;
}


